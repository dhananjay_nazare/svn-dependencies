#! /usr/bin/env node
'use strict'

const cmdr = require('commander');

const VERSION_NUMBER = '0.1.0'

cmdr.version(VERSION_NUMBER)
  .command('clean', 'clear SVN cache and pacakge.json file')
  .command('setup', 'alternative to install command with different installation steps')
  .parse(process.argv)

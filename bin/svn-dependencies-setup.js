#! /usr/bin/env node
'use strict'

const program = require('commander');
const projectRoot = require('app-root-path').toString();
const logger = require('../lib/logging');
const npm = require('../lib/npm');
const svn = require('../lib/svn');
const svnLess = require('../lib/svn-less');

program
  .option('-C, --cache', 'Cache SVN dependencies and their references in package.json file')
  .option('-L, --lessGlob', 'Register SVN less dependencies')
  .parse(process.argv);

svn.fetch_deps_from_svn_async(projectRoot).then((svnModulesInfo) => {
  if (!svnModulesInfo.svnDependencies || svnModulesInfo.svnDependencies.length === 0) {
    logger.warn('No SVN dependencies found');
    process.exit(0);
  }

  npm.register_svn_dependencies(svnModulesInfo.svnDependencies, projectRoot);

  npm.remove_scope_folder(projectRoot);
  
  npm.npm_install(projectRoot);

  if (program.lessGlob) {
    svnLess.register_svn_less_dependencies(svnModulesInfo.svnRelations);
  }

  if (!program.cache) {
    logger.info(`Cleaning SVN cache...`);
    npm.clean_svn_dependencies(projectRoot);
    logger.info(`Cleaned SVN cache successfully!!`);
  }

  process.exit(0);
}, (err) => {
  npm.clean_svn_dependencies(projectRoot);
  logger.error(`Installation of SVN dependencies failed!!`);
  process.exit(1);
});
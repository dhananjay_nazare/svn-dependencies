#! /usr/bin/env node
'use strict'

const projectRoot = require('app-root-path').toString();
const npm = require('../lib/npm');
const logger = require('../lib/logging');

logger.info(`Cleaning SVN cache...`);
npm.clean_svn_dependencies(projectRoot);
logger.info(`Cleaned SVN cache successfully!!`);

process.exit(0);
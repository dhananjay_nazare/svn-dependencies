'use strict'

const chalk = require('chalk');
const MODULE_NAME = 'svn-depdendencies';
module.exports = { debug, success, info, error, warn };

function debug(line) {
  if (process.env.NODE_ENV === 'production')
    return;
  console.log(`${MODULE_NAME} ${chalk.bgBlue.white('DEBUG')} ${line}`);
}

function success(line) {
  console.log(`${MODULE_NAME} ${chalk.bgGreen.black('OK')} ${line}`);
}

function info(line) {
  console.log(`${MODULE_NAME} ${line}`);
}

function warn(line) {
  console.log(`${MODULE_NAME} ${chalk.bgYellow.black('WARN')} ${line}`);
}

function error(line) {
  console.log(`${MODULE_NAME} ${chalk.bgRed.white('ERROR')} ${line}`);
}

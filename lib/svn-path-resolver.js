const svnVInfo = require("svn-version-info");

module.exports = { resolve };

function resolve(moduleName, svnUrl) {
  if (
    !svnUrl ||
    svnUrl.indexOf("@") < 0 ||
    svnUrl.lastIndexOf("@") < svnUrl.lastIndexOf("/")
  ) {
    return svnUrl;
  }

  if (
    !svnUrl ||
    svnUrl.indexOf("@") <= 0 ||
    svnUrl.indexOf("@") === svnUrl.length - 1
  ) {
    throw "Invalid SVN path!!";
  }

  let version = svnUrl.split(/@+/).pop();
  if (version !== "latest" && version.split(".").length !== 3) {
    throw "Invalid Version!!";
  }

  svnUrl = svnUrl.slice(0, svnUrl.lastIndexOf("@"));
  moduleName = moduleName.replace("@", "").replace("/", "-");

  let vInfo = svnVInfo.getSvnVersionInfo(svnUrl);

  if (version === "latest") {
    version = vInfo.getLatestVersion();
  } else if (version.indexOf("^") == 0) {
    version = vInfo.getLatestMinorVersion(version.slice(1, version.length));
  } else if (version.indexOf("~") == 0) {
    version = vInfo.getLatestPatchVersion(version.slice(1, version.length));
  }

  if (vInfo.isZippedVersion(version)) {
    return `${svnUrl}/tags/${moduleName}-${version}.tgz`;
  }
  return `${svnUrl}/tags/${version}`;
}
